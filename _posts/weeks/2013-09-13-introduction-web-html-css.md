---
title: Introduction Web, HTML & CSS
week: 01
hw: Have a resume ready (if you don't have one yet, don't worry just cobble something together with fake data). Start converting it into a web page. Use the tags and basic setup we used today. If you run into problems, make a note of them and bring them to class next week.
readings: 
  - title: Is HTML a programming language?
    link: http://www.cs.tut.fi/~jkorpela/prog.html
  - title: New structural elements in HTML5
    link: http://dev.opera.com/articles/view/new-structural-elements-in-html5
categories:
- week
---
