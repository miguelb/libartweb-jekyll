---
title: Workflows
week: 03
readings: 
  - title: Creative Floating (MySLC)
  - title: Flexible Text (MySLC)

hw: Create the markup and layout for this <a class="bold" href="http://cl.ly/Rakj">design</a>.<br>Download this <a class="bold" href="http://cl.ly/Rcb5">zip </a>to get started.<br><br><span class="label step-up label-danger">Due Tuesday, October 1st, 9:00pm</span>

categories:
- week
---
