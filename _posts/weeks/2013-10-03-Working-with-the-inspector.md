---
title: Review CSS Positioning & Working with the Inspector
week: 04
hw: <a href="http://www.codecademy.com/tracks/web">Codecademy's Web Fundamentals</a> Lessons, <a href="http://bit.ly/1by9Uhz">Intro to CSS Positioning</a>, <a href="http://bit.ly/1byaNXs">Advanced CSS Positioning</a>  
readings: 
  - title: Handcrafted CSS - Modular Floating Management (MySLC)
readingpending: false
categories:
- week
---
